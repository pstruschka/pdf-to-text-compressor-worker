defmodule Worker.App do
  def main(_args \\ []) do
    {:ok, connection} = case System.get_env("RABBIT_HOST") do
             nil -> AMQP.Connection.open
             host -> AMQP.Connection.open("amqp://guest:guest@" <> host)
           end
    {:ok, channel} = AMQP.Channel.open(connection)

    AMQP.Queue.declare(channel, "compress", durable: true)
    AMQP.Basic.qos(channel, prefetch_count: 1)
    AMQP.Basic.consume(channel, "compress")
    AMQP.Queue.declare(channel, "update_status", durable: true)
    IO.puts " [x] Awaiting work"

    Worker.wait_for_messages(channel)
  end
end
