defmodule Worker do
  @moduledoc """
  Documentation for Worker.
  """

  defp cleanup(bucket, files, object) do
    files
    |> Enum.each(fn file -> File.rm!(file) end)
    File.rm!(object)
    System.cmd("mc", ["rm", "--recursive", "--force", "myminio/#{bucket}"])
    IO.puts "Cleaned up #{bucket}"
  end

  defp download_txt(bucket, object) do
    System.cmd("mc", ["cp", "myminio/#{bucket}/#{object}", object])
    IO.puts "Downloaded #{object} from #{bucket}"
  end

  defp upload_tar(bucket, object) do
    System.cmd("mc", ["cp", object, "myminio/#{bucket}/#{object}"])
    IO.puts "Uploaded #{object} to #{bucket}"
  end

  defp stat_object?(bucket, object) do
    case System.cmd("mc", ["stat", "myminio/#{bucket}/#{object}"]) do
      {_, 0} -> true
      _ -> false
    end
  end

  defp compress(files, object) do
    :erl_tar.create(object, Enum.map(files, &to_charlist/1), [:compressed])
  end

  defp get_files(bucket, files) do
    {complete, incomplete} = files
    |> Enum.split_with(fn x -> stat_object?(bucket, x) end)

    complete
    |> Enum.each(fn x -> download_txt(bucket, x) end)

    :timer.sleep(3000)
    case incomplete do
      [] -> :ok
      files -> get_files(bucket, files)
    end
  end

  defmodule CompressTask do
    @derive [Poison.Encoder]
    defstruct [:bucket, :intermediate_bucket,:object, :targets]
  end

  defmodule StatusComplete do
    @derive [Poison.Encoder]
    defstruct [:bucket, :object, :status]
  end

  defmodule Status do
    @derive [Poison.Encoder]
    defstruct [:bucket, :status]
  end

  def wait_for_messages(channel) do
    receive do
      {:basic_deliver, payload, meta} ->
        IO.puts " [.] got message #{payload}"


        json = Poison.decode!(payload, as: %CompressTask{})

        get_files(json.intermediate_bucket, json.targets)

        compress(json.targets, json.object)

        AMQP.Basic.publish(channel,
          "",
          "update_status",
          Poison.encode!(%Status{bucket: json.bucket, status: "compressed"}))

        IO.puts " Compressed and created #{json.object}"

        upload_tar(json.bucket, json.object)
        AMQP.Basic.publish(channel,
          "",
          "update_status",
          Poison.encode!(%StatusComplete{bucket: json.bucket, object: json.object, status: "complete"}))

        IO.puts " Complete and uploaded #{json.object}"

        cleanup(json.intermediate_bucket, json.targets, json.object)

        AMQP.Basic.ack(channel, meta.delivery_tag)

        wait_for_messages(channel)
    end
  end
end
