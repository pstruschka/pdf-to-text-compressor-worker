defmodule Worker.MixProject do
  use Mix.Project

  def project do
    [
      app: :compressor_worker,
      version: "0.1.0",
      elixir: "~> 1.7",
      escript: [main_module: Worker.App],
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :amqp, :poison],
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ranch_proxy_protocol,
       override: true,
       git: "https://github.com/heroku/ranch_proxy_protocol.git",
       ref: "4e0f73a385f37cc6f277363695e91f4fc7a81f24"},
      {:amqp, "~> 1.0"},
      {:poison, "~> 3.1"},
    ]
  end
end
