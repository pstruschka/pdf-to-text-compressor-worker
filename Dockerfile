FROM elixir:alpine

RUN apk add git

WORKDIR /app

ADD https://dl.minio.io/client/mc/release/linux-amd64/mc /usr/local/bin/
RUN chmod +x /usr/local/bin/mc

ADD https://raw.githubusercontent.com/eficode/wait-for/master/wait-for /usr/local/bin/
RUN chmod +x /usr/local/bin/wait-for

RUN mix local.hex --force
RUN mix local.rebar --force

COPY mix.exs .

RUN mix deps.get

COPY . .

RUN MIX_ENV=prod mix escript.build

CMD ./entry_point.sh
